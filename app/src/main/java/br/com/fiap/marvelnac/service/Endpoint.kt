package br.com.fiap.marvelnac.service

import br.com.fiap.marvelnac.character.CharacterDataWrapper
import retrofit2.http.GET
import retrofit2.http.Query
import java.security.MessageDigest

const val publicKey = "60a569baf9d59f6d44d3225f3bcbd804"
const val privateKey = "d6b31e3e380bb2bcd52782e7e06e02cc2465e58a"

interface Endpoint {
    @GET("v1/public/characters")
    suspend fun getCharacter(
        @Query("apikey") apikey: String,
        @Query("ts") ts: String,
        @Query("hash") hash: String
    ): CharacterDataWrapper
}

fun getApiHash(ts: String): String {
    val bytes = MessageDigest
        .getInstance("MD5")
        .digest("${ts}${ privateKey}${ publicKey}".toByteArray())

    return bytes.joinToString("") {"%02x".format(it)}
}