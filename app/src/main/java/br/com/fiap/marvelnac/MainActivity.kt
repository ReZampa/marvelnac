package br.com.fiap.marvelnac

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.fiap.marvelnac.character.CharacterViewModel
import br.com.fiap.marvelnac.character.CharactersAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private val viewModel: CharacterViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel.getCharacters()

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

        viewModel.characterLiveData.observe(this, Observer {
            Log.e("JSON", viewModel.characterLiveData.toString())
            progress_bar.visibility = View.GONE
            recyclerView.adapter = CharactersAdapter(it)
        })
    }
}