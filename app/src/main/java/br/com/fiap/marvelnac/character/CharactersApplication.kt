package br.com.fiap.marvelnac.character

import android.app.Application
import br.com.fiap.marvelnac.service.Endpoint
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CharactersApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@CharactersApplication)
            modules(characterModule)
        }
    }

    private val characterModule = module {
        single {provideRetrofit()}
        single {provideEndpoint(get())}
        viewModel {CharacterViewModel(get())}
    }

    private fun provideEndpoint(retrofit: Retrofit) : Endpoint {
        return retrofit.create(Endpoint::class.java)
    }

    private fun provideRetrofit() : Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://gateway.marvel.com:443/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}