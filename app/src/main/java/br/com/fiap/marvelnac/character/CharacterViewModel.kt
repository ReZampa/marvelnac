package br.com.fiap.marvelnac.character

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.fiap.marvelnac.service.Endpoint
import br.com.fiap.marvelnac.service.getApiHash
import br.com.fiap.marvelnac.service.publicKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class CharacterViewModel(private val service: Endpoint): ViewModel() {
    val characterLiveData = MutableLiveData<List<Character>>()

    fun getCharacters() {
        viewModelScope.launch(Dispatchers.IO) {
            val ts = Date().time.toString()
            val hash = getApiHash(ts)

            val apiResult: CharacterDataWrapper = service.getCharacter(publicKey, ts, hash )

            characterLiveData.postValue(apiResult.data?.results)
        }
    }
}