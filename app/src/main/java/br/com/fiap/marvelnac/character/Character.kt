package br.com.fiap.marvelnac.character

class CharacterDataWrapper(
    val data: CharacterDataContainer? = null
)

class CharacterDataContainer(
    val results: List<Character>? = null
)

data class Character(
    val id: Int? = null,
    val name: String? = null,
    val description: String? = null,
    val thumbnail: Photos? = null
)

class Photos(
    val path: String? = null,
    val extension: String? = null
)