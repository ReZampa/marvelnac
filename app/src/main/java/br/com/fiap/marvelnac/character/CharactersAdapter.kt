package br.com.fiap.marvelnac.character

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.fiap.marvelnac.R
import com.bumptech.glide.Glide

class CharactersAdapter(val list: List<Character>): RecyclerView.Adapter<CharacterViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.character_item, parent, false)
        return CharacterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        return holder.bind(list[position])
    }
}

class CharacterViewHolder(itemView : View): RecyclerView.ViewHolder(itemView) {
    private val thumbnail:ImageView = itemView.findViewById(R.id.ch_thumbnail)
    private val id: TextView = itemView.findViewById(R.id.ch_Id)
    private val name: TextView = itemView.findViewById(R.id.ch_Name)
    private val description: TextView = itemView.findViewById(R.id.ch_Description)

    @SuppressLint("SetTextI18n")
    fun bind(ch: Character) {
        Glide.with(itemView.context).load("${ch.thumbnail?.path}.${ch.thumbnail?.extension}").into(thumbnail)
        id.text = "ID: ${ch.id}"
        name.text = "NAME: ${ch.name}"
        description.text = "DESCRIPTION:\n${ch.description}"
    }
}